import { createContext, useReducer } from 'react'
const AppReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_EXPENSE':
      return { ...state, expenses: [action.payload, ...state.expenses] }
    case 'REMOVE_EXP':
      return {
        ...state,
        expenses: state.expenses.filter((e) => e.id !== action.payload),
      }
    case 'EDIT_BUDGET':
      return { ...state, budget: action.payload }
    default:
      return state
  }
}
const initiatlState = {
  budget: 2000,
  expenses: [
    { id: 11, name: 'travel', cost: 40 },
    { id: 31, name: 'courses', cost: 60 },
  ],
}

export const AppContext = createContext()

export const AppProvider = (props) => {
  const [state, dispatch] = useReducer(AppReducer, initiatlState)

  return (
    <AppContext.Provider
      value={{
        budget: state.budget,
        expenses: state.expenses,
        dispatch,
      }}
    >
      {props.children}
    </AppContext.Provider>
  )
}
