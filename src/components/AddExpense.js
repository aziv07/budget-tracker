import React, { useContext, useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { AppContext } from '../context/AppContext'
const AddExpense = () => {
  const { dispatch } = useContext(AppContext)
  const [name, setName] = useState('')
  const [cost, setCost] = useState('')
  const subForm = (e) => {
    e.preventDefault()
    const expense = { name, cost, id: uuidv4() }
    setCost('')
    setName('')
    dispatch({ type: 'ADD_EXPENSE', payload: expense })
  }
  return (
    <form onSubmit={subForm}>
      <div className='row'>
        <div className='col-sm'>
          {' '}
          <label for='name'>
            <input
              id='name'
              value={name}
              onChange={(e) => setName(e.target.value)}
              required
              className='form-control'
              type='text'
            />
          </label>
        </div>
        <div className='col-sm'>
          <label for='cost'>
            <input
              value={cost}
              pattern='[0-9]+.[0-9]+$'
              id='cost'
              onChange={(e) => setCost(e.target.value)}
              required
              className='form-control'
              type='text'
            />
          </label>
        </div>
        <div className='col-sm'>
          <input
            id='btn'
            className='form-control'
            type='submit'
            value='Save'
            className='btn btn-success'
          />
        </div>
      </div>
    </form>
  )
}

export default AddExpense
