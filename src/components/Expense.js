import React, { useContext } from 'react'
import { TiDelete } from 'react-icons/ti'
import { AppContext } from '../context/AppContext'
const Expense = ({ name, cost, id }) => {
  const { dispatch } = useContext(AppContext)
  const remove = (e) => {
    e.preventDefault()
    dispatch({ type: 'REMOVE_EXP', payload: id })
  }
  return (
    <li className='list-group-item d-flex justify-content-between align-items-center'>
      <span className='col-10'>{name}</span>
      <div className='badge badge-primary  badge-pill mr-3'>${cost}</div>
      <TiDelete
        onClick={remove}
        style={{ cursor: 'pointer' }}
        size='1.5em'
      ></TiDelete>
    </li>
  )
}

export default Expense
