import React, { useContext } from 'react'
import { AppContext } from '../context/AppContext'

const Remaining = () => {
  const { budget, expenses } = useContext(AppContext)
  const tot = expenses.reduce((prev, curr) => prev + parseInt(curr.cost), 0)
  return (
    <div
      className={tot > budget ? 'alert alert-danger' : 'alert alert-success'}
    >
      <span>Remaining: ${budget - tot}</span>
    </div>
  )
}

export default Remaining
