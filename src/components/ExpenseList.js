import React, { useContext } from 'react'
import { AppContext } from '../context/AppContext'
import Expense from './Expense'
const ExpenseList = () => {
  const { expenses } = useContext(AppContext)

  return (
    <ul className='list-group'>
      {expenses.map((e) => (
        <Expense key={e.id} id={e.id} name={e.name} cost={e.cost} />
      ))}
    </ul>
  )
}

export default ExpenseList
