import React, { useContext, useState } from 'react'
import { AppContext } from '../context/AppContext'

const Budget = () => {
  const [edit, setEdit] = useState(false)
  const { budget, dispatch } = useContext(AppContext)
  const [val, setVal] = useState(budget)
  const submitChanges = (e) => {
    e.preventDefault()
    setEdit(false)
    dispatch({ type: 'EDIT_BUDGET', payload: val })
  }
  return (
    <div className='alert alert-secondary'>
      {edit ? (
        <>
          <input
            type='number'
            name='budget'
            value={val}
            onChange={(e) => setVal(e.target.value)}
          />{' '}
          <button className='btn btn-warning' onClick={submitChanges}>
            Save Change
          </button>
        </>
      ) : (
        <>
          {' '}
          <span>Budget: ${budget}</span>{' '}
          <button className='btn btn-primary' onClick={(e) => setEdit(true)}>
            Edit
          </button>
        </>
      )}
    </div>
  )
}

export default Budget
