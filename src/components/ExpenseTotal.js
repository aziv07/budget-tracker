import React, { useContext } from 'react'
import { AppContext } from '../context/AppContext'

const ExpenseTotal = () => {
  const { expenses } = useContext(AppContext)
  return (
    <div className='alert alert-primary'>
      <span>
        Spent so far: $
        {expenses.reduce((prev, curr) => prev + parseInt(curr.cost), 0)}
      </span>
    </div>
  )
}

export default ExpenseTotal
